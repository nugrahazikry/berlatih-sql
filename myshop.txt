1. Membuat database

C:\Users\Zikry\xampp\mysql\bin>mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 58
Server version: 10.4.17-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> create database myshop;
Query OK, 1 row affected (0.002 sec)

MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| myshop             |
| mysql              |
| ngetest            |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
7 rows in set (0.001 sec)


2. Membuat table di dalam database

MariaDB [(none)]> use mysql;
Database changed
MariaDB [mysql]> create table users(
    -> id int auto_increment,
    -> name varchar(255),
    -> primary key(id),
    -> email varchar(255),
    -> password varchar(255)
    -> );
Query OK, 0 rows affected (0.454 sec)

MariaDB [mysql]> describe users;
+----------+--------------+------+-----+---------+----------------+
| Field    | Type         | Null | Key | Default | Extra          |
+----------+--------------+------+-----+---------+----------------+
| id       | int(11)      | NO   | PRI | NULL    | auto_increment |
| name     | varchar(255) | YES  |     | NULL    |                |
| email    | varchar(255) | YES  |     | NULL    |                |
| password | varchar(255) | YES  |     | NULL    |                |
+----------+--------------+------+-----+---------+----------------+
4 rows in set (0.013 sec)

MariaDB [mysql]> create table categories(
    -> id int auto_increment,
    -> name varchar(255),
    -> primary key(id)
    -> );
Query OK, 0 rows affected (0.478 sec)

MariaDB [mysql]> describe categories;
+-------+--------------+------+-----+---------+----------------+
| Field | Type         | Null | Key | Default | Extra          |
+-------+--------------+------+-----+---------+----------------+
| id    | int(11)      | NO   | PRI | NULL    | auto_increment |
| name  | varchar(255) | YES  |     | NULL    |                |
+-------+--------------+------+-----+---------+----------------+
2 rows in set (0.018 sec)

MariaDB [mysql]>

MariaDB [mysql]> create table items(
    -> id int auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int,
    -> stock int,
    -> category_id int(11),
    -> primary key(id),
    -> foreign key (category_id) references categories(id)
    -> );
Query OK, 0 rows affected (0.733 sec)

MariaDB [mysql]> describe items;
+-------------+--------------+------+-----+---------+----------------+
| Field       | Type         | Null | Key | Default | Extra          |
+-------------+--------------+------+-----+---------+----------------+
| id          | int(11)      | NO   | PRI | NULL    | auto_increment |
| name        | varchar(255) | YES  |     | NULL    |                |
| description | varchar(255) | YES  |     | NULL    |                |
| price       | int(11)      | YES  |     | NULL    |                |
| stock       | int(11)      | YES  |     | NULL    |                |
| category_id | int(11)      | YES  | MUL | NULL    |                |
+-------------+--------------+------+-----+---------+----------------+
6 rows in set (0.050 sec)


3. Memasukkan data pada table

MariaDB [mysql]> insert into users values (
    -> 1,"John Doe","john@doe.com","john123")
    -> ;
Query OK, 1 row affected (0.049 sec)

MariaDB [mysql]> insert into users values (
    -> 2,"Jane Doe","jane@doe.com","jenita123")
    -> ;
Query OK, 1 row affected (0.135 sec)

MariaDB [mysql]> select * from users;
+----+----------+--------------+-----------+
| id | name     | email        | password  |
+----+----------+--------------+-----------+
|  1 | John Doe | john@doe.com | john123   |
|  2 | Jane Doe | jane@doe.com | jenita123 |
+----+----------+--------------+-----------+
2 rows in set (0.001 sec)

MariaDB [mysql]> insert into categories(name) values ("gadget"),("cloth"),("men"),("women"),("branded");
Query OK, 5 rows affected (0.097 sec)
Records: 5  Duplicates: 0  Warnings: 0

MariaDB [mysql]> select * from categories;
+----+---------+
| id | name    |
+----+---------+
|  1 | gadget  |
|  2 | cloth   |
|  3 | men     |
|  4 | women   |
|  5 | branded |
+----+---------+
5 rows in set (0.001 sec)

MariaDB [mysql]> insert into items values (
    -> 1,"Sumsang b50","hape keren dari merek sumsang",4000000,100,1)
    -> ;
Query OK, 1 row affected (0.101 sec)

MariaDB [mysql]> insert into items values (
    -> 2,"Uniklooh","baju keren dari brand ternama",500000,50,2)
    -> ;
Query OK, 1 row affected (0.068 sec)

MariaDB [mysql]> insert into items values (
    -> 3,"IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1)
    -> ;
Query OK, 1 row affected (0.042 sec)

MariaDB [mysql]> select*from items;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 |
|  2 | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
3 rows in set (0.001 sec)


4. Mengambil data dari Database

MariaDB [mysql]> select id,name,email from users;
+----+----------+--------------+
| id | name     | email        |
+----+----------+--------------+
|  1 | John Doe | john@doe.com |
|  2 | Jane Doe | jane@doe.com |
+----+----------+--------------+
2 rows in set (0.001 sec)

MariaDB [mysql]> select * from items where price > 1000000;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
2 rows in set (0.028 sec)

MariaDB [mysql]> select * from items where name like '%uniklo%';
+----+----------+-------------------------------+--------+-------+-------------+
| id | name     | description                   | price  | stock | category_id |
+----+----------+-------------------------------+--------+-------+-------------+
|  2 | Uniklooh | baju keren dari brand ternama | 500000 |    50 |           2 |
+----+----------+-------------------------------+--------+-------+-------------+
1 row in set (0.000 sec)

MariaDB [mysql]> select items.name, items.description, items.price, items.stock, items.category_id, categories.name from items join
    -> categories on items.category_id = categories.id;
+-------------+-----------------------------------+---------+-------+-------------+--------+
| name        | description                       | price   | stock | category_id | name   |
+-------------+-----------------------------------+---------+-------+-------------+--------+
| Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 | gadget |
| Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 | cloth  |
| IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 | gadget |
+-------------+-----------------------------------+---------+-------+-------------+--------+
3 rows in set (0.001 sec)



5. Mengubah data dari database
MariaDB [mysql]> update items set price=2500000 where name="Sumsang b50";
Query OK, 1 row affected (0.081 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [mysql]> select * from items;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 2500000 |   100 |           1 |
|  2 | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
3 rows in set (0.000 sec)
